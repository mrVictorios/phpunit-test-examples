# PHPUnit Test Examples

## Description

This is a collection of proven testing acts to get you code in a good way tested.
Keep in mind that exists multiple ways to solve problems with there pros and cons, its on
you to decide wich way is the best for you but also accept other solutions for the 
same problem.

## Examples Content
1. Unit Testing
    * Testing Getter and Setter by responsibility
        * Testing getSet Types without default value
        * Testing getSet Types with default value
    * Testing Getter and Setter together
        * Testing getSet Types without default value
        * Testing getSet Types with default value
    * Dependency Injection
    * Dependency Container Injection
    * Trait
        * Public method
        * Protected method
    * Singleton Pass through
  
## Important

If you find issues in any case (thinking, gramma etc.) please feel free to comment and contribute.

