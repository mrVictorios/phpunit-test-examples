# PHPStorm Live Template

## Live Template Test Setter and Getter
### Description
Write Getter and Setter Snippet with Bool property.

### Snippet
```php
public function testSet$CLASS_FIELD_CC$()
{
    $this->$OBJECT$->set$CLASS_FIELD_CC$($TEST_VALUE$);

    $this->assertAttributeEquals($TEST_VALUE$, '$CLASS_FIELD$', $this->$OBJECT$);

    return $this->$OBJECT$;
}

/**
 * @param $CLASS$ $$$OBJECT$
 * @depends testSet$CLASS_FIELD_CC$
 */
public function testIs$CLASS_FIELD_CC$($CLASS$ $$$OBJECT$)
{
    $this->assert$TEST_VALUE_CC$($$$OBJECT$->is$CLASS_FIELD_CC$());
}
$END$

```
### Template Variables

| VARIABLE | EXPRESSION | DEFAULT VALUE | SKIP IF DEFINED |
| :------- | :--------- | :-----------: | --------------: |
| CLASS_FIELD_CC | capitalize(String) 
| OBJECT
| TEST_VALUE
| CLASS_FIELD
| CLASS | capitalize($CLASS_NAME$)
| Test_VALUE_CC | capitalize($TEST_VALUE$)

## Live Template Test Bool Setter and Getter 