<?php

namespace Examples\UnitTesting\GetterSetterResponsibility;

class MyClass
{
    /** @var bool */
    private $bool;
    /** @var bool */
    private $boolWithDefault = false;
    /** @var int */
    private $int;
    /** @var float */
    private $float;
    /** @var string */
    private $string;
    /** @var array */
    private $array;
    /** @var object */
    private $object;

    /**
     * @return bool
     */
    public function isBool(): bool
    {
        return $this->bool;
    }

    /**
     * @param bool $bool
     */
    public function setBool(bool $bool): void
    {
        $this->bool = $bool;
    }

    /**
     * @return bool
     */
    public function isBoolWithDefault(): bool
    {
        return $this->boolWithDefault;
    }

    /**
     * @param bool $boolWithDefault
     */
    public function setBoolWithDefault(bool $boolWithDefault): void
    {
        $this->boolWithDefault = $boolWithDefault;
    }

    /**
     * @return int
     */
    public function getInt(): int
    {
        return $this->int;
    }

    /**
     * @param int $int
     */
    public function setInt(int $int): void
    {
        $this->int = $int;
    }

    /**
     * @return float
     */
    public function getFloat(): float
    {
        return $this->float;
    }

    /**
     * @param float $float
     */
    public function setFloat(float $float): void
    {
        $this->float = $float;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * @param string $string
     */
    public function setString(string $string): void
    {
        $this->string = $string;
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->array;
    }

    /**
     * @param array $array
     */
    public function setArray(array $array): void
    {
        $this->array = $array;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param \stdClass $object
     */
    public function setObject(\stdClass $object): void
    {
        $this->object = $object;
    }
}