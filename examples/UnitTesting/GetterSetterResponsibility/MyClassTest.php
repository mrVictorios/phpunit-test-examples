<?php

namespace Examples\UnitTesting\GetterSetterResponsibility;

use PHPUnit\Framework\TestCase;


/**
 * with phpunit 8 deprecated and will not work with phpunit 9
 */
class MyClassTest extends TestCase
{
    /**
     * @var MyClass
     *
     * @reason
     *      The class to test should stored in a property in test case. For Readability the name should be equals
     *      to the class.
     */
    private $myClass;

    protected function setUp()
    {
        $this->myClass = new MyClass();
    }

    /**
     * @return MyClass
     *
     * @reason
     *      1. The method name, should contain the name an the target of the test.
     */
    public function testSetBool_shouldStoreValueInProperty(): MyClass
    {
        $this->myClass->setBool(true);

        $this->assertAttributeEquals(true, 'bool', $this->myClass);

        return $this->myClass;
    }

    /**
     * @param MyClass $myClass
     * @depends testSetBool_shouldStoreValueInProperty
     *
     * @reason
     *      1.  Getter test should depend on the "setter" test. Optimal way the property is only set over the setter
     *          Method when setter not work it makes no sens to test a getter
     *      2.  You want test the Getter not the Setter, but you need a value in the property to test. Depending on
     *          resolve this in a easy way without injection.
     */
    public function testGetBool_shouldReturnValueInProperty(MyClass $myClass): void
    {
        $this->assertTrue($myClass->isBool());
    }

    /**
     * @return MyClass
     *
     * @reason
     *      1.  On Properties with default values you should first test that returns the default value.
     */
    public function testGetBoolWithDefault_shouldReturnDefaultValue(): MyClass
    {
        $this->assertFalse($this->myClass->isBoolWithDefault());

        return $this->myClass;
    }

    /**
     * @param MyClass $myClass
     * @return MyClass
     * @depends testGetBoolWithDefault_shouldReturnDefaultValue
     *
     * @reason
     *      1.  On Test setter on a property with default. You should test that you property has no more the
     *          default value. This help you to prevent useless tests
     */
    public function testSetBoolWithDefault_shouldStoreValueInProperty(MyClass $myClass): MyClass
    {
        $myClass->setBoolWithDefault(true);

        $this->assertAttributeNotEquals(false, 'boolWithDefault', $myClass);
        $this->assertAttributeEquals(true, 'boolWithDefault', $myClass);

        return $myClass;
    }

    /**
     * @param MyClass $myClass
     * @depends testSetBoolWithDefault_shouldStoreValueInProperty
     *
     * @reason look at testSetBool_shouldStoreValueInProperty()
     */
    public function testGetBoolWithDefault_shouldReturnValue(MyClass $myClass): void
    {
        $this->assertTrue($myClass->isBoolWithDefault());
    }

    public function testSetInt(): MyClass
    {
        $this->myClass->setInt(101);

        $this->assertAttributeEquals(101, 'int', $this->myClass);

        return $this->myClass;
    }

    /**
     * @param MyClass $myClass
     * @depends testSetInt
     */
    public function testGetInt(MyClass $myClass): void
    {
        $this->assertEquals(101, $myClass->getInt());
    }

    public function testSetFloat(): MyClass
    {
        $this->myClass->setFloat(3.14);

        $this->assertAttributeEquals(3.14, 'float', $this->myClass);

        return $this->myClass;
    }

    /**
     * @param MyClass $myClass
     * @depends testSetFloat
     */
    public function testGetFloat(MyClass $myClass): void
    {
        $this->assertEquals(3.14, $myClass->getFloat());
    }

    public function testSetObject(): MyClass
    {
        $this->myClass->setObject(new \stdClass());

        $this->assertAttributeEquals(new \stdClass(), 'object', $this->myClass);

        return $this->myClass;
    }

    /**
     * @param MyClass $myClass
     * @depends testSetObject
     */
    public function testGetObject(MyClass $myClass): void
    {
        $this->assertEquals(new \stdClass(), $myClass->getObject());
    }

    public function testSetString(): MyClass
    {
        $this->myClass->setString('my string');

        $this->assertAttributeEquals('my string', 'string', $this->myClass);

        return $this->myClass;
    }

    /**
     * @param MyClass $myClass
     * @depends testSetString
     */
    public function testGetString(MyClass $myClass): void
    {
        $this->assertEquals('my string', $myClass->getString());
    }

    public function testSetArray(): MyClass
    {
        $this->myClass->setArray([ 'my array' ]);

        $this->assertAttributeEquals([ 'my array' ], 'array', $this->myClass);

        return $this->myClass;
    }

    /**
     * @param MyClass $myClass
     * @depends testSetArray
     */
    public function testGetArray(MyClass $myClass): void
    {
        $this->assertEquals([ 'my array' ], $myClass->getArray());
    }
}