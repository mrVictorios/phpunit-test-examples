# PHPStorm Live Template

## Live Template Test Setter and Getter
### Description
Write Getter and Setter Snippet.

### Snippet
```php
public function testSet$CLASS_FIELD_CC$()
{
    $this->$OBJECT$->set$CLASS_FIELD_CC$($TEST_VALUE$);

    $this->assertAttributeEquals($TEST_VALUE$, '$CLASS_FIELD$', $this->$OBJECT$);

    return $this->$OBJECT$;
}

/**
 * @param $CLASS$ $$$OBJECT$
 * @depends testSet$CLASS_FIELD_CC$
 */
public function testGet$CLASS_FIELD_CC$($CLASS$ $$$OBJECT$)
{
    $this->assertEquals($TEST_VALUE$, $$$OBJECT$->get$CLASS_FIELD_CC$());
}
$END$
```
### Template Variables

| VARIABLE | EXPRESSION | DEFAULT VALUE | SKIP IF DEFINED |
| :------- | :--------- | :-----------: | --------------: |
| OBJECT
| CLASS_FIELD
| CLASS_FIELD_CC | capitalize(String) 
| CLASS
| TEST_VALUE

## Live Template Test Bool Setter and Getter 