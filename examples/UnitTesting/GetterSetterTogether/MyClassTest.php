<?php

namespace Examples\UnitTesting\GetterSetterTogether;

use PHPUnit\Framework\TestCase;

class MyClassTest extends TestCase
{
    /**
     * @var MyClass
     *
     * @reason
     *      The class to test should stored in a property in test case. For Readability the name should be equals
     *      to the class.
     */
    private $myClass;

    protected function setUp()
    {
        $this->myClass = new MyClass();
    }

    public function testSetBool_shouldStoreValueInProperty()
    {
        $this->myClass->setBool(true);

        $this->assertTrue($this->myClass->isBool());
    }

    public function testGetBoolWithDefault_shouldReturnDefaultValue()
    {
        $this->assertFalse($this->myClass->isBoolWithDefault());

        $this->myClass->setBoolWithDefault(true);

        $this->assertTrue($this->myClass->isBoolWithDefault());
    }

    public function testSetInt()
    {
        $this->myClass->setInt(101);

        $this->assertEquals(101, $this->myClass->getInt());
    }

    public function testSetFloat()
    {
        $this->myClass->setFloat(3.14);

        $this->assertEquals(3.14, $this->myClass->getFloat());
    }

    public function testSetObject()
    {
        $this->myClass->setObject(new \stdClass());

        $this->assertEquals(new \stdClass(), $this->myClass->getObject());
    }

    public function testSetString()
    {
        $this->myClass->setString('my string');
        $this->assertEquals('my string',  $this->myClass->getString());
    }

    public function testSetArray()
    {
        $this->myClass->setArray(['my array']);

        $this->assertEquals(['my array'], $this->myClass->getArray());
    }
}