<?php

namespace Examples\UnitTesting\DependencyInjection;

class IHaveDependencies
{
    /** @var SomeOtherClassInterface */
    private $someOtherClass;

    /**
     * IHaveDependencies constructor.
     * @param SomeOtherClassInterface $someOtherClass
     */
    public function __construct(SomeOtherClassInterface $someOtherClass)
    {
        $this->someOtherClass = $someOtherClass;
    }

    /**
     * @return array
     */
    public function getTodo():array
    {
        return $this->someOtherClass->todo();
    }
}
