<?php

namespace Examples\UnitTesting\DependencyInjection;

use PHPUnit\Framework\TestCase;

class IHaveDependenciesTest extends TestCase
{
    /*
     * @description Example with Dependencies
     */
    public function test__construct_shouldStoreDependencyInProperty():void
    {
        $someOtherClass = $this->createMock(SomeOtherClassInterface::class);

        $iHaveDependency = new IHaveDependencies($someOtherClass);

        $this->assertAttributeEquals($someOtherClass, 'someOtherClass', $iHaveDependency);
    }

    /**
     * @testing_hint TEST the Object Responsibility not the implementation!
     */
    public function testGetTodo_shouldCallSomeOtherClassMethod(): void
    {
        $someOtherClass = $this->createMock(SomeOtherClassInterface::class);

        $iHaveDependency = new IHaveDependencies($someOtherClass);

        $someOtherClass
            ->expects($this->atLeastOnce())
            ->method('todo')
            ->willReturn(['my awesome todo'])
        ;

        $this->assertEquals(['my awesome todo'], $iHaveDependency->getTodo());
    }
}
