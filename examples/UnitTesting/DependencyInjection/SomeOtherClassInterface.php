<?php

namespace Examples\UnitTesting\DependencyInjection;

interface SomeOtherClassInterface
{
    /**
     * @return array
     */
    public function todo(): array;
}