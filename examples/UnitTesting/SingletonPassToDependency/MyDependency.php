<?php

namespace Examples\UnitTesting\SingletonPassToDependency;

Interface MyDependency
{
    /**
     * @param MySingleton $singleton
     * @return void
     */
    public function doSomething(MySingleton $singleton);
}