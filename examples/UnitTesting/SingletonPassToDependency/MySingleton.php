<?php

namespace Examples\UnitTesting\SingletonPassToDependency;

class MySingleton
{
    /** @var array */
    private $data;

    private function __construct()
    {
    }

    /**
     * @return static
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @param array $data
     * @return void
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}