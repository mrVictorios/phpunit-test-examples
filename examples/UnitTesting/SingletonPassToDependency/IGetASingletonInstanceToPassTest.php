<?php

namespace Examples\UnitTesting\SingletonPassToDependency;

use PHPUnit\Framework\TestCase;

class IGetASingletonInstanceToPassTest extends TestCase
{
    /** @var $iGetASingletonInstanceToPass */
    private $iGetASingletonInstanceToPass;

    /**
     * @reason we does not have a easy method to mock a singleton instance and we not want to manipulate our production
     *         code so we have not much option. The best way in my opinion is to ignore the singleton in such case and
     *         handle it like a new in a method and test that the passing object is configured as expected at this point
     *         yes in such case we have a component- and not a unit-test because we test also the singleton code. But we
     *         had a readable and comprehensible code that is more important then solve general naming patterns.
     *
     *         The method "iDoIt" form "iGetASingletonInstanceToPass" gets a singleton instance configure it and pass it
     *         to his dependency without returning value. So we had to test that the method solve his responsibility
     *         the only way at this point is to check the passing argument for the expected method call of the
     *         dependency. In this case "doSomething" in the object "MyDependency" with the method "logicalAnd" we can
     *         check multiple aspects of the argument in this case we check that is an instance of MySingleton and this
     *         object Attribute "data", a other way to check this without knowledge of the private things of
     *         the singleton is in next case.
     */
    public function testIDoIt_shouldPassMySingletonToDependency_checkWithAttribute()
    {
        $singletonGetDataAtPass = null;
        $myDependency = $this->createMock(MyDependency::class);

        $this->iGetASingletonInstanceToPass = new IGetASingletonInstanceToPass(
            $myDependency
        );

        $myDependency
            ->expects($this->atLeastOnce())
            ->method('doSomething')
            ->with(
                $this->logicalAnd(
                    $this->isInstanceOf(MySingleton::class),
                    $this->attributeEqualTo(
                        'data',
                        [
                            'url' => 'https://www.example.de/this-is-an-example-de',
                        ]
                    )
                )
            )
        ;

        $this->iGetASingletonInstanceToPass->iDoIt();
    }

    /**
     * @reason  a another way to resolve the Problem as describe in the case annotation before is. To check the passing
     *          with a callback. On this way you does not need private information about the singleton to check it, that
     *          is better but the test is also less readable and less comprehensible on the first read. Booth ways are
     *          helps you to test the code its depend on you is better for your case.
     */
    public function testIDoIt_shouldPassMySingletonToDependency_checkWithCallback()
    {
        $singletonGetDataAtPass = null;
        $myDependency = $this->createMock(MyDependency::class);

        $this->iGetASingletonInstanceToPass = new IGetASingletonInstanceToPass(
            $myDependency
        );

        $myDependency
            ->expects($this->atLeastOnce())
            ->method('doSomething')
            ->with(
                $this->callback((function (MySingleton $mySingleton) {
                    return $this
                        ->equalTo([
                            'url' => 'https://www.example.de/this-is-an-example-de',
                        ])
                        ->evaluate($mySingleton->getData())
                    ;
                })->bindTo($this, $this))
            )
        ;

        $this->iGetASingletonInstanceToPass->iDoIt();
    }
}
