<?php

namespace Examples\UnitTesting\SingletonPassToDependency;

class IGetASingletonInstanceToPass
{
    private $myDependency;

    public function __construct(MyDependency $myDependency)
    {
        $this->myDependency = $myDependency;
    }

    public function iDoIt()
    {
        $singleton = MySingleton::create();

        $singleton->setData([
            'url' => 'https://www.example.de/this-is-an-example-de'
        ]);

        $this->myDependency->doSomething($singleton);
    }
}