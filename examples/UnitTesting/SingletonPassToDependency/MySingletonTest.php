<?php


namespace Examples\UnitTesting\SingletonPassToDependency;

use PHPUnit\Framework\TestCase;

class MySingletonTest extends TestCase
{

    public function testCreate()
    {
        $this->assertInstanceOf(MySingleton::class, MySingleton::create());
    }

    public function testSetData()
    {
        $mySingleton = MySingleton::create();

        $mySingleton->setData([
            'my things' => 'hi',
        ]);

        $this->assertAttributeEquals(
            ['my things' => 'hi'],
            'data',
            $mySingleton
        );

        return $mySingleton;
    }

    /**
     * @depends testSetData
     * @param MySingleton $mySingleton
     */
    public function testGetData(MySingleton $mySingleton)
    {
        $this->assertEquals(
            ['my things' => 'hi'], $mySingleton->getData()
        );
    }
}
