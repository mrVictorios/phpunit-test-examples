<?php

namespace Examples\UnitTesting\UseTraits;

use PHPUnit\Framework\TestCase;

class MyTraitExampleTest extends TestCase
{
    /**
     * @var MyTraitExample
     *
     * @reason
     *      The trait to test should stored in a property in test case. For Readability the name should be equals
     *      to the trait.
     */
    private $myTraitExample;

    /**
     *
     * @reason      this creates a mock with a custom method "callProtectedMethod". CallProtectedMethod
     *              is a proxy method to call the protected method of the trait.
     *
     *              When you has no protected methods in the trait to test you can use
     *
     *                  $this->myTraitExample = $this->getMockForTrait(Class::name);
     *
     *              that is shorter and more readable. Do bot use the getMockForTraitArguments to Configure
     *              the mock that is less readable.
     */
    public function testPublicAction_shouldReturnAString()
    {
        $this->myTraitExample = $this->getMockForTrait(MyTraitExample::class);

        $this->assertEquals(
            'my trait does something with the argument',
            $this->myTraitExample->publicAction('do this')
        );
    }


    /**
     * @reason  testing protected class methods makes no sense in the most cases, but by a trait it can be the purpose
     *          why the trait exits. You should never use reflaction to manipulate the subject that you want to test.
     *          To Test the protected method we add a proxy method to the Mock thats call the protected method of the
     *          trait. Then we call the proxy method to test our protected method. By this way we not manipulate
     *          production code or change the visibility of the method for test reason.
     */
    public function testProtectedMethod_shouldAdd1337()
    {
        $this->myTraitExample = $this
            ->getMockBuilder(MyTraitExample::class)
            ->setMethods(['callProtectedMethod'])
            ->getMockForTrait()
        ;

        $methodCall = function (int $intArg) {
            return $this->protectedMethod($intArg);
        };

        $this->myTraitExample
            ->method('callProtectedMethod')
            ->willReturnCallback(
                $methodCall->bindTo($this->myTraitExample, $this->myTraitExample)
            )
        ;

        $this->assertEquals(1338, $this->myTraitExample->callProtectedMethod(1));
    }
}
