<?php

namespace Examples\UnitTesting\UseTraits;

trait MyTraitExample
{
    /**
     * @param string $argument
     * @return string
     */
    public function publicAction(string $argument) {
        return 'my trait does something with the argument';
    }

    /**
     * @param int $intArg
     * @return int
     */
    protected function protectedMethod(int $intArg)
    {
        return $intArg + 1337;
    }
}