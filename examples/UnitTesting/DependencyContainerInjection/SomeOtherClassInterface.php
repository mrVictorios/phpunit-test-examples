<?php

namespace Examples\UnitTesting\DependencyContainerInjection;

interface SomeOtherClassInterface
{
    /**
     * @return array
     */
    public function todo(): array;
}