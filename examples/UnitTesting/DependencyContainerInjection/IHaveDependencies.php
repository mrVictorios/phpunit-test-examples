<?php

namespace Examples\UnitTesting\DependencyContainerInjection;

use Psr\Container\ContainerInterface;

class IHaveDependencies
{
    /** @var SomeOtherClassInterface */
    private $someOtherClass;

    /**
     * IHaveDependencies constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        /*
         * problem on this usage? will we become waht we expected?
         */
        $this->someOtherClass = $container->get('some_other_class');
    }

    /**
     * @return array
     */
    public function getTodo():array
    {
        return $this->someOtherClass->todo();
    }
}
