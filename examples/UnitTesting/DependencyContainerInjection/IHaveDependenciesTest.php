<?php

namespace Examples\UnitTesting\DependencyContainerInjection;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class IHaveDependenciesTest extends TestCase
{
    /*
     * @description Example with Dependency Injection Container Injected
     */
    public function test__construct_shouldStoreDependencyInProperty(): void
    {
        $someOtherClass = $this->createMock(SomeOtherClassInterface::class);
        $container      = $this->createMock(ContainerInterface::class);

        $container
            ->expects($this->atLeastOnce())
            ->method('get')
            ->willReturnMap([
                ['some_other_class', $someOtherClass],
            ])
        ;

        $iHaveDependency = new IHaveDependencies($container);

        $this->assertAttributeEquals($someOtherClass, 'someOtherClass', $iHaveDependency);
    }

    /**
     * @testing_hint TEST the Object Responsibility not the implementation!
     */
    public function testGetTodo_shouldCallSomeOtherClassMethod(): void
    {
        $someOtherClass = $this->createMock(SomeOtherClassInterface::class);
        $container      = $this->createMock(ContainerInterface::class);

        $container
            ->expects($this->atLeastOnce())
            ->method('get')
            ->willReturnMap([
                ['some_other_class', $someOtherClass],
            ])
        ;

        $iHaveDependency = new IHaveDependencies($container);

        $someOtherClass
            ->expects($this->atLeastOnce())
            ->method('todo')
            ->willReturn(['my awesome todo'])
        ;

        $this->assertEquals(['my awesome todo'], $iHaveDependency->getTodo());
    }
}
